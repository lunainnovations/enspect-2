import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // freeCorrosion: [
    //   "Add New",
    //   "4130 Steel",
    //   "1008 Steel",
    //   "AA7075-T6",
    //   "AA2024-T3",
    //   "AA6061-T6",
    //   "EN988 Zinc",
    // ],
    // galvanicCorrosion: [
    //   "Add New",
    //   "AA7075-T6 / A286",
    //   "AA7075-T6 / 316SS",
    //   "AA7075-T6 / Ti6-4",
    //   "AA7075-T6 / 4130 Steel",
    //   "AA7075-T6 / CFRP",
    //   "AA2024-T3 / A286",
    //   "AA2024-T3 / Ti6-4",
    //   "AA2024-T3 / 4130 Steel",
    //   "AA6061-T6 / Ti6-4",
    //   "EN988 Zinc / 316SS",
    //   "EN988 Zinc / 1008 Steel",
    // ],
    freeCorrosionAlloy: "",
    freeCorrosion: [],
    galvanicCorrosion: [],

    dataDistribution: [
      "Distribution A",
      "Distribution B",
      "Distribution C",
      "Distribution D",
      "Distribution E",
      "Commercial Confidential",
    ],

    preTreatment: [
      "Alodine T5900",
      "Bonderite C-IC 33",
      "Bonderite C-IC 79",
      "3M AC-130-2",
      "PreKote",
      "Bonderite M-CR 1200S",
      "Bonderite M-CR 1201",
      "Bonderite M-CR T5900 ",
      "Surtec 650V",
    ],

    primer: [
      "PPG 02GN084",
      "PPG 44GN098",
      "PPG 02Y040A",
      "PPG CA7233",
      "PPG 44GN072",
      "Hentzen AD9318",
      "PPG EWAE118",
      "PPG PR-1432GV",
      "PPG 45GY005",
      "Randolph LN4811",
    ],

    topcoat: [
      "PPG 99GY001",
      "PPG CA8202 (F17925 Gloss White)",
      "Desothane HS CA9311 F36118",
      "PPG / Deft 03W127A",
      "PPG 03GY444",
      "Cor-Ban 35",
      "CorrosionX",
    ],

    deviceTypes: ["LS", "DG", "CR"],
    types: ["Laboratory", "Outdoor", "On Asset"],
    sensorDataFilters: [
      "AirTemp",
      "RH",
      "SurfaceTemp",
      "LowFreq",
      "HighFreq",
      "GalvCorr",
      "FreeCorr",
    ],

    sensorMeasurement: false,
    crSensorAllow: true,
    measurementInputs: {},
    coatingInputs: {},

    snackbar: {
      show: false,
      text: "",
    },

    //required input component
    laboratory: false,
    outdoor: false,
    onAsset: false,
    fileState: true,
    outerLoading: false,
    innerLoading: false,
    finishedLoadingMessage: "Data submitted correctly",
    finishedLoading: false,
    FileName: "",
    FileId: "",

    returnedSensorData: {},
    returnedChannelSensorData: {},
    responeArray: [],
    responeChannelArray: [],
    graphData: [],
    sensorDeviceCount: 0,
    isTwoYaxis: false,
    deviceOptions: [],
    tableDataOptions: [],
    
    localUrl : 'http://10.10.32.109:9000/Luna-api/',

    devurl : 'https://localhost:7089/Luna-api/',

    localDevUrl: 'http://10.10.32.127:8550/Luna-api/',
  },
  //methods that can't change data in state. you can use async code here. but not in mutations. dispatch action
  actions: {
    async getLastInsertedId({ commit }, data) {
      commit("getLastInsertedId", data);
    },
    async passAlloyMeasurements({ commit }, payload) {
      this.state.freeCorrosionAlloy = payload.freeCorrosionAlloy;
    },
    //Get corrosion values list
    async getCorrosionValues({ commit }) {

      await axios
        .get(this.state.localUrl + "AcuityData/get-corrosion-list/")
        .then((response) => {
          commit("addCorrosionValue", response.data);
        })
        .catch((error) => console.log(error));
    },
    async addCorrosionValue({ commit }, payload) {
      //create new FormdData object to send to server
      //append the file and the user data

      let isFreeCorrosion = true;
      //we need the value and if it's free or galvani
      if (payload.valueType === "Galvanic Corrosion") {
        isFreeCorrosion = false;
      }

      let formData = new FormData();
      formData.append("corrosionVal", JSON.stringify(payload.value));
      formData.append("isFreeCorrosion", isFreeCorrosion);

      console.log(payload.valueType, isFreeCorrosion);

      await axios
        .post(this.state.localUrl + 'AcuityData/update-corrosion', formData, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          commit("addCorrosionValue", response.data);
        })
        .catch((error) => {
          alert(error);
        });

      setTimeout(function () {
        let status = "OK";
        if (status === "OK") {
          commit(
            "showSnackbar",
            payload.value +
              " added and sent to " +
              payload.valueType +
              " db file"
          );
        }
      }, 300);
    },
    async getDataTableOperations({ commit }) {
    
      await axios
        .get(this.state.localUrl + 'AcuityData/get-data-table-operations')
        .then((response) => {
          commit("addDataTableOperations", response.data);
        })
        .catch((error) => console.log(error))
    },
    async getSensorChannelData({ commit }, sensorPayload) {
      let formData = new FormData();
      formData.append("sensorData", JSON.stringify(sensorPayload));
      console.log(sensorPayload);

     
      await axios
        .post(this.state.localUrl+'AcuityData/get-channel-data', formData, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          console.log(response);
          commit("getUserChannelData", response);
          commit("testPlotGraphData");
        })
        .catch((error) => {
          console.log(error);
          if (error.response.status === 404) {
            alert(error.response.data.Message);
          }
        });
    },
    async createCsv({ commit }, payload) {

    
      await axios
        .post(this.state.localUrl+'AcuityData/return-csv-sensor-data', payload.Id, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", payload.SensorType + "_" + payload.DeviceSerialNumber + ".csv");
          document.body.appendChild(link);
          link.click();
        })
        .catch((error) => {
          console.log(error);
        });
    },
    async deleteSensorData({ commit, dispatch }, payload) {
      console.log(payload);
      let formData = new FormData();
      formData.append("sensorData", JSON.stringify(payload));
      let  localUrl = this.state.localUrl + 'AcuityData/delete-sensor-data';
      await axios
        .post(localUrl, formData, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          console.log(response.data);
          dispatch("getDataTableOperations");
          // commit("addDataTableOperations", response.data);
        })
        .catch((error) => {
          console.log(error);
          if (error.response.status === 404) {
            alert(error.response.data.Message);
          }
        });
    },

    async getSensorData({ commit }, sensorPayload) {
      let formData = new FormData();
      formData.append("sensorData", JSON.stringify(sensorPayload));
      
     let  localUrl = this.state.localUrl + 'AcuityData/get-sensor-data';
      await axios
        .post(localUrl, formData, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          console.log(response);
          commit("getUserSensorData", response);
        })
        .catch((error) => {
          console.log(error);
          if (error.response.status === 404) {
            alert(error.response.data.Message);
          }
        });
    },
    async showLoader({ commit }) {
      await commit("showLoader");
    },
  },
  mutations: {
    selectExposureType(state, exposureType) {
      console.log("here is the type", exposureType);
      if (exposureType == "Laboratory") {
        state.laboratory = true;
        state.outdoor = false;
        state.onAsset = false;
      } else if (exposureType == "Outdoor") {
        state.outdoor = true;
        state.onAsset = false;
        state.laboratory = false;
      } else if (exposureType == "") {
        state.outdoor = false;
        state.onAsset = false;
        state.laboratory = false;
      } else {
        state.laboratory = false;
        state.outdoor = false;
        state.onAsset = true;
      }
    },
    showSnackbar(state, text) {
      let timeout = 0;
      if (state.snackbar.show) {
        state.snackbar.show = false;
        timeout = 250;
      }
      setTimeout(() => {
        state.snackbar.show = true;
        state.snackbar.text = text;
      }, timeout);
    },
    hideSnackbar(state) {
      state.snackbar.show = false;
    },
    addCorrosionValue(state, payload) {
      (this.state.freeCorrosion = payload.FreeCorrosion),
        (this.state.galvanicCorrosion = payload.GalvanicCorrosion);
    },
    addDataTableOperations(state, payload) {
      state.tableDataOptions = payload;
    },
    showLoader(state) {
      state.innerLoading = true;
      state.outerLoading = true;
    },
    getLastInsertedId(state, data) {
      state.innerLoading = false;
      state.finishedLoading = true;
      state.FileId = data.File_Id;
      state.FileName = data.FileName;
    },
    getUserChannelData(state, response) {
      state.returnedChannelSensorData = {
        Time: response.data[0].UnixTime,
        DeviceSerialNumber: response.data[0].DeviceSerialNumber,
        SensorData: response.data[0].SensorDataSelection,
        SensorData2: response.data[0].SensorDataSelection2,
      };
      state.responeChannelArray.push(state.returnedChannelSensorData);
      console.log(
        "get user channel data method results",
        state.responeChannelArray
      );
    },

    getUserSensorData(state, response) {
      state.returnedSensorData = response;

      if (Object.keys(state.returnedSensorData.data) < 1) {
        state.returnedSensorData = {
          key: 0,
          CollectionId: state.returnedSensorData.data[0].Id,
          DeviceSerialNumber:
            state.returnedSensorData.data[0].DeviceSerialNumber,
          Time: state.returnedSensorData.data[0].UnixTime,
          SensorData: state.returnedSensorData.data[0].SensorDataSelection,
          SensorData2: state.returnedSensorData.data[0].SensorDataSelection2,
        };
        state.responeArray.push(state.returnedSensorData);
      } else {
        for (var key in response.data) {
          if (response.data.hasOwnProperty(key)) {
            state.returnedSensorData = {
              key: key,
              CollectionId: response.data[key].Id,
              DeviceSerialNumber: response.data[key].DeviceSerialNumber,
              Time: response.data[key].UnixTime,
              SensorData: response.data[key].SensorDataSelection,
              SensorData2: response.data[key].SensorDataSelection2,
            };
            state.responeArray.push(state.returnedSensorData);
            // console.log(key);
            // console.log(response.data[key].DeviceSerialNumber);
            // console.log(response.data[key].UnixTime);
          } else {
          }
        }
      }

      // Object.entries(response).forEach((entry) => {
      //   const [key, value] = entry;
      //   console.log(`${key}: ${value}`);
      // });
    },

    testPlotGraphData(state) {
      if (state.responeChannelArray.length < 11) {
        let xAxis = [];
        xAxis =
          state.responeChannelArray[state.responeChannelArray.length - 1].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let yAxis = [];
        yAxis =
          state.responeChannelArray[state.responeChannelArray.length - 1]
            .SensorData;

        var trace1 = {
          x: xAxis,
          y: yAxis,
          name: state.responeChannelArray[state.responeChannelArray.length - 1].DeviceSerialNumber,
          type: "scatter"
        };
        state.graphData.push(trace1);
      }else{
        alert("Only 10 Devices Can Be Viewed at One Time")
      }

      console.log("test plot", state.graphData);
    },

    plotGraphData(state) {
      state.graphData = [];

      if (state.responeChannelArray.length === 1) {
        let xAxis = [];
        xAxis = state.responeChannelArray[0].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let yAxis = [];
        yAxis = state.responeChannelArray[0].SensorData;

        if (state.isTwoYaxis) {
          // yAxis.sort((a, b) => b.value - a.value);

          var trace1 = {
            x: xAxis,
            y: yAxis,
            name: "# " + state.responeChannelArray[0].DeviceSerialNumber,
            type: "scatter",
          };

          let yAxis2 = [];
          yAxis2 = state.responeChannelArray[0].SensorData2;

          var trace2 = {
            x: xAxis,
            y: yAxis2,
            name:
              "Y Axis 2 # " + state.responeChannelArray[0].DeviceSerialNumber,
            xaxis: "x",
            yaxis: "y2",
            type: "scatter",
          };
          state.graphData.push(trace1, trace2);
        } else {
          // alert(state.isTwoYaxis)
          var trace1 = {
            x: xAxis,
            y: yAxis,
            name:
            "Device Serial # " +
            state.responeChannelArray[0].DeviceSerialNumber,
            type: "scatter",
          };
          state.graphData.push(trace1);
        }

        state.sensorDeviceCount = state.responeChannelArray.length;
      } else if (state.responeChannelArray.length === 2) {
        let trace1Xaxis = [];
        trace1Xaxis = state.responeChannelArray[0].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace1Yaxis = [];
        trace1Yaxis = state.responeChannelArray[0].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace2Xaxis = [];
        trace2Xaxis = state.responeChannelArray[1].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace2Yaxis = [];
        trace2Yaxis = state.responeChannelArray[1].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        if (state.isTwoYaxis) {
          var trace1 = {
            x: trace1Xaxis,
            y: trace1Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[0].DeviceSerialNumber,
            type: "scatter",
          };

          var trace2 = {
            x: trace2Xaxis,
            y: trace2Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[1].DeviceSerialNumber,
            type: "scatter",
          };

          let yAxis2 = [];
          yAxis2 = state.responeChannelArray[0].SensorData2;

          let yAxis3 = [];
          yAxis3 = state.responeChannelArray[1].SensorData2;

          var yTrace1 = {
            x: xAxis,
            y: yAxis2,
            name:
              "Y Axis 2 # " + state.responeChannelArray[0].DeviceSerialNumber,
            xaxis: "x",
            yaxis: "y2",
            type: "scatter",
          };
          var yTrace2 = {
            x: xAxis,
            y: yAxis2,
            name:
              "Y Axis 2 # " + state.responeChannelArray[0].DeviceSerialNumber,
            xaxis: "x",
            yaxis: "y3",
            type: "scatter",
          };
          state.graphData.push(trace1, trace2, yTrace1, yTrace2);
        }
        var trace1 = {
          x: trace1Xaxis,
          y: trace1Yaxis,
          name:
            "Device Serial # " +
            state.responeChannelArray[0].DeviceSerialNumber,
          type: "scatter",
        };

        var trace2 = {
          x: trace2Xaxis,
          y: trace2Yaxis,
          name:
            "Device Serial # " +
            state.responeChannelArray[1].DeviceSerialNumber,
          type: "scatter",
        };

        state.graphData.push(trace1, trace2);
      } else if (state.responeChannelArray.length === 3) {
      } else if (state.responeChannelArray.length === 4) {
      } else if (state.responeChannelArray.length === 5) {
      } else if (state.responeChannelArray.length === 6) {
      } else if (state.responeChannelArray.length === 7) {
      } else if (state.responeChannelArray.length === 8) {
      } else if (state.responeChannelArray.length === 9) {
        let trace1Xaxis = [];
        trace1Xaxis = state.responeArray[0].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace1Yaxis = [];
        trace1Yaxis = state.responeArray[0].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace2Xaxis = [];
        trace2Xaxis = state.responeArray[1].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace2Yaxis = [];
        trace2Yaxis = state.responeArray[1].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace3Xaxis = [];
        trace3Xaxis = state.responeArray[2].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace3Yaxis = [];
        trace3Yaxis = state.responeArray[2].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace4Xaxis = [];
        trace4Xaxis = state.responeArray[3].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace4Yaxis = [];
        trace4Yaxis = state.responeArray[3].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace5Xaxis = [];
        trace5Xaxis = state.responeArray[4].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace5Yaxis = [];
        trace5Yaxis = state.responeChannelArray[4].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace6Xaxis = [];
        trace6Xaxis = state.responeChannelArray[5].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace6Yaxis = [];
        trace6Yaxis = state.responeChannelArray[5].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace7Xaxis = [];
        trace7Xaxis = state.responeChannelArray[6].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace7Yaxis = [];
        trace7Yaxis = state.responeChannelArray[6].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace8Xaxis = [];
        trace8Xaxis = state.responeChannelArray[7].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace8Yaxis = [];
        trace8Yaxis = state.responeChannelArray[7].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace9Xaxis = [];
        trace9Xaxis = state.responeChannelArray[8].Time;

        if (state.isTwoYaxis) {
        } else {
          var trace1 = {
            x: trace1Xaxis,
            y: trace1Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[0].DeviceSerialNumber,
            type: "scatter",
          };

          var trace2 = {
            x: trace2Xaxis,
            y: trace2Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[1].DeviceSerialNumber,
            type: "scatter",
          };

          var trace3 = {
            x: trace3Xaxis,
            y: trace3Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[2].DeviceSerialNumber,
            type: "scatter",
          };

          var trace4 = {
            x: trace4Xaxis,
            y: trace4Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[3].DeviceSerialNumber,
            type: "scatter",
          };

          var trace5 = {
            x: trace5Xaxis,
            y: trace5Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[4].DeviceSerialNumber,
            type: "scatter",
          };

          var trace6 = {
            x: trace6Xaxis,
            y: trace6Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[5].DeviceSerialNumber,
            type: "scatter",
          };

          var trace7 = {
            x: trace7Xaxis,
            y: trace7Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[6].DeviceSerialNumber,
            type: "scatter",
          };

          var trace8 = {
            x: trace8Xaxis,
            y: trace8Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[7].DeviceSerialNumber,
            type: "scatter",
          };

          var trace9 = {
            x: trace9Xaxis,
            y: trace9Yaxis,
            name:
              "Device Serial # " +
              state.responeChannelArray[8].DeviceSerialNumber,
            type: "scatter",
          };

          state.sensorDeviceCount = 9;
          state.graphData.push(
            trace1,
            trace2,
            trace3,
            trace4,
            trace5,
            trace6,
            trace7,
            trace8,
            trace9
          );
        }
      } else {
        let trace1Xaxis = [];
        trace1Xaxis = state.responeArray[0].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace1Yaxis = [];
        trace1Yaxis = state.responeArray[0].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace2Xaxis = [];
        trace2Xaxis = state.responeArray[1].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace2Yaxis = [];
        trace2Yaxis = state.responeArray[1].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace3Xaxis = [];
        trace3Xaxis = state.responeArray[2].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace3Yaxis = [];
        trace3Yaxis = state.responeArray[2].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace4Xaxis = [];
        trace4Xaxis = state.responeArray[3].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace4Yaxis = [];
        trace4Yaxis = state.responeArray[3].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace5Xaxis = [];
        trace5Xaxis = state.responeArray[4].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace5Yaxis = [];
        trace5Yaxis = state.responeArray[4].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace6Xaxis = [];
        trace6Xaxis = state.responeArray[5].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace6Yaxis = [];
        trace6Yaxis = state.responeArray[5].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace7Xaxis = [];
        trace7Xaxis = state.responeArray[6].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace7Yaxis = [];
        trace7Yaxis = state.responeArray[6].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace8Xaxis = [];
        trace8Xaxis = state.responeArray[7].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace8Yaxis = [];
        trace8Yaxis = state.responeArray[7].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace9Xaxis = [];
        trace9Xaxis = state.responeArray[8].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace9Yaxis = [];
        trace9Yaxis = state.responeArray[8].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        let trace10Xaxis = [];
        trace10Xaxis = state.responeArray[9].Time;
        // xAxis.sort((a, b) => b.date - a.date)
        let trace10Yaxis = [];
        trace10Yaxis = state.responeArray[9].SensorData;
        // yAxis.sort((a, b) => b.value - a.value);

        if (state.isTwoYaxis) {
        } else {
          var trace1 = {
            x: trace1Xaxis,
            y: trace1Yaxis,
            name: "Device Serial # " + state.responeArray[0].DeviceSerialNumber,
            type: "scatter",
          };

          var trace2 = {
            x: trace2Xaxis,
            y: trace2Yaxis,
            name: "Device Serial # " + state.responeArray[1].DeviceSerialNumber,
            type: "scatter",
          };

          var trace3 = {
            x: trace3Xaxis,
            y: trace3Yaxis,
            name: "Device Serial # " + state.responeArray[2].DeviceSerialNumber,
            type: "scatter",
          };

          var trace4 = {
            x: trace4Xaxis,
            y: trace4Yaxis,
            name: "Device Serial # " + state.responeArray[3].DeviceSerialNumber,
            type: "scatter",
          };

          var trace5 = {
            x: trace5Xaxis,
            y: trace5Yaxis,
            name: "Device Serial # " + state.responeArray[4].DeviceSerialNumber,
            type: "scatter",
          };

          var trace6 = {
            x: trace6Xaxis,
            y: trace6Yaxis,
            name: "Device Serial # " + state.responeArray[5].DeviceSerialNumber,
            type: "scatter",
          };

          var trace7 = {
            x: trace7Xaxis,
            y: trace7Yaxis,
            name: "Device Serial # " + state.responeArray[6].DeviceSerialNumber,
            type: "scatter",
          };

          var trace8 = {
            x: trace8Xaxis,
            y: trace8Yaxis,
            name: "Device Serial # " + state.responeArray[7].DeviceSerialNumber,
            type: "scatter",
          };

          var trace9 = {
            x: trace9Xaxis,
            y: trace9Yaxis,
            name: "Device Serial # " + state.responeArray[8].DeviceSerialNumber,
            type: "scatter",
          };

          var trace10 = {
            x: trace10Xaxis,
            y: trace10Yaxis,
            name: "Device Serial # " + state.responeArray[9].DeviceSerialNumber,
            type: "scatter",
          };
          state.sensorDeviceCount = 10;
          state.graphData.push(
            trace1,
            trace2,
            trace3,
            trace4,
            trace5,
            trace6,
            trace7,
            trace8,
            trace9,
            trace10
          );
        }
      }
    },
  },
  modules: {},
});

/* --------------------------------


reader.onload  = function(ev){
        var fileArray = this.result.split(/\r\n|\n/);
            for(var line = 0; line < fileArray.length-1; line++){
              if(fileArray[line].startsWith(SearchString)){
                alert("yes it does")
                 setTimeout(() => {
                    this.$emit("load", line + " --> "+ fileArray[line]);
                  }, 10);
                  
              }
          console.log(line + " --> "+ fileArray[line]);

          */
