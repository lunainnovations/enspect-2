import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import vuelidate from 'vuelidate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import qs from 'qs';

Vue.config.productionTip = false
Vue.use(vuelidate)
Vue.use(VueAxios, axios)
Vue.use(qs)

new Vue({
  router,
  store,
  vuetify,
  axios,
  vuelidate,
  qs,
  render: h => h(App)
}).$mount('#app')
