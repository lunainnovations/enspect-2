export function graphData() {
 

  // var trace2 = {
  //   x: [
  //     "02/01/2021",
  //     "02/04/2021",
  //     "02/05/2021",
  //     "02/07/2021",
  //     "02/8/2021",
  //     "02/09/2021",
  //     "02/10/2021",
  //     "02/11/2021",
  //   ],
  //   y: [16, 5, 11, 9],
  //   type: "scatter",
  // };

  

  
 
  
 

  // console.log("Insdie data js", payload.exposureType, payload.sensorDataName,  payload.dataSelections[0].minValue,  payload.dataSelections[0].maxValue);
  // let traceData = sensorData(payload)

  // //convert unixTime into dates array
  // let convertedUnixTimeData = convertDates(traceData[0])

  // //now pass that converted array to another function that will get the selected date range. 
  //  let unixTimeData =  getDates(payload.dates, convertedUnixTimeData);
  //  unixTimeData.length =  quicklyMakeAnotherArray(traceData[1]).length
  //  console.log(unixTimeData.length, quicklyMakeAnotherArray(traceData[1]).length)


  var trace1 = {
    x:  this.$store.state.returnedSensorData.Time,
    y:  this.$store.state.returnedSensorData.SensorDataSelection,
    type: "scatter",
  };


  var trace = [trace1, layout];
  return trace;
}


function quicklyMakeAnotherArray(traceData){

  let convertedValues = [];
  for(var i=0; i<traceData[0].length; i++){
     const element = traceData[0][i]; //gets unixTime element
     convertedValues.push(element)

  }
 return convertedValues
 
}

function getDates(dates, unixTimeData){
//search the converted Unix time array based on the dates 
  return unixTimeData.filter(temp => temp >= dates[0] && temp <= dates[1])

}



function sensorData(payload) {
  //I'll need to create two data sets within this data1 object
  if (payload.sensorDataName === "Relative Humidity") {
    payload.sensorDataName = "rh";
  } else if (payload.sensorDataName === "Air Temp") {
    payload.sensorDataName = "airTemp";
  }
  console.log(payload.sensorDataName);
  let data = [
    {
      exposureType: "Outdoor",
      unixTime : [
        1632513660, 1632515460, 1632517260, 1632519060, 1632520860, 1632522660,
        1632524460, 1632526260, 1632528060, 1632529860, 1632531660, 1632533460,
        1632535260, 1632537060, 1632538860, 1632540660, 1632542460, 1632544260,
        1632546060, 1632547860, 1632549660, 1632551460, 1632553260, 1632555060,
        1632556860, 1632558660, 1632560460, 1632562260, 1632564060, 1632565860,
        1632567660, 1632569460, 1632571260, 1632573060, 1632574860, 1632576660,
        1632578460, 1632580260, 1632582060, 1632583860, 1632585660, 1632587460,
        1632589260, 1632591060, 1632592860, 1632594660, 1632596460, 1632598260,
        1632600060, 1632601860, 1632603660, 1632605460, 1632607260, 1632609060,
        1632610860, 1632612660, 1632614460, 1632616260, 1632618060, 1632619860,
        1632621660, 1632623460, 1632625260, 1632627060, 1632628860, 1632630660,
        1632632460, 1632634260, 1632636060, 1632637860, 1632639660, 1632641460,
        1632643260, 1632645060, 1632646860, 1632648660, 1632650460, 1632652260,
        1632654060, 1632655860, 1632657660, 1632659460, 1632661260, 1632663060,
        1632664860, 1632666660, 1632668460, 1632670260, 1632672060, 1632673860,
        1632675660, 1632677460, 1632679260, 1632681060, 1632682860, 1632684660,
        1632686460, 1632688260, 1632690060, 1632691860, 1632693660, 1632695460,
        1632697260, 1632699060, 1632700860, 1632702660, 1632704460, 1632706260,
        1632708060, 1632709860, 1632711660, 1632713460, 1632715260, 1632717060,
        1632718860, 1632720660, 1632722460, 1632724260, 1632726060, 1632727860,
        1632729660, 1632731460, 1632733260, 1632735060, 1632736860, 1632738660,
        1632740460, 1632742260, 1632744060,
      ], //August 24th 2021 - 27th
      rh : [ 
        45.52, 46.08, 45.48, 44.91, 45.3, 45.82, 47.76, 48.45, 50.16, 51, 50.98,
        52.8, 52.97, 53.91, 54.05, 54.27, 54.25, 54.27, 54.25, 54.29, 54.31,
        54.18, 54.07, 54.09, 54.01, 53.91, 53.85, 54.18, 53.96, 54.07, 53.99,
        53.39, 53.01, 53.53, 54.05, 54.9, 55.54, 56.2, 57.37, 56.91, 57.34,
        57.33, 56.26, 55.45, 54.81, 54.42, 53.81, 53.75, 53.48, 53.44, 53.75,
        53.73, 53.57, 52.99, 53.93, 54.14, 54.71, 54.94, 55.8, 56.24, 55.38,
        56.65, 55.79, 55.21, 55.02, 54.51, 54.18, 53.92, 53.75, 53.58, 53.56,
        53.41, 53.63, 53.63, 53.48, 53.89, 53.98, 53.87, 53.59, 53.09, 53.03,
        52.92, 53.59, 53.92, 53.69, 53.38, 52.8, 52.02, 51.16, 50.53, 50, 49.4,
        49.14, 48.84, 48.39, 48.23, 48.22, 48.06, 48.04, 48.33, 47.93, 47.77,
        48.03, 47.91, 49.77, 50.09, 50.98, 50.72, 51.36, 51.78, 52.08, 52.36,
        52.69, 52.91, 52.86, 53.03, 52.88, 53.12, 53.19, 53.16, 52.99,

        52.16, 51.15, 50.62, 50.33, 59.31, 67.55, 58.25, 62.4,
      ],
      airTemp : [
        22.5, 22.39, 22.35, 22.25, 22.23, 22.19, 22.12, 22.02, 21.97, 21.9,
        21.88, 21.87, 21.8, 21.8, 21.7, 21.63, 21.53, 21.42, 21.3, 21.18, 21.06,
        20.94, 20.79, 20.7, 20.57, 20.41, 20.24, 20.19, 20.13, 20.06, 20.06,
        20.06, 20.02, 19.96, 19.93, 19.92, 19.91, 19.91, 19.93, 20, 20.08, 20.2,
        20.32, 20.43, 20.57, 20.68, 20.81, 20.97, 21.08, 21.25, 21.39, 21.53,
        21.64, 21.76, 21.78, 21.8, 21.85, 21.87, 21.9, 21.88, 21.87, 21.78,
        21.76, 21.67, 21.6, 21.52, 21.43, 21.33, 21.23, 21.1, 21.05, 20.95,
        20.87, 20.78, 20.68, 20.61, 20.51, 20.5, 20.48, 20.46, 20.37, 20.34,
        20.26, 20.24, 20.2, 20.22, 20.2, 20.27, 20.33, 20.4, 20.46, 20.58,
        20.68, 20.82, 20.97, 21.09, 21.27, 21.42, 21.58, 21.64, 21.78, 21.81,
        21.82, 21.87, 21.85, 21.85, 21.88, 21.9, 21.9, 21.85, 21.8, 21.71,
        21.63, 21.53, 21.44, 21.35, 21.25, 21.15, 21.02, 20.92, 20.84, 20.7,
        20.61, 20.5, 20.44, 20.39, 20.39, 20.34, 20.4,
      ],
    },
    {
      exposureType: "Laboratory",
      unixTime: [
        1632513660, 1632515460, 1632517260, 1632519060, 1632520860, 1632522660,
        1632524460, 1632526260, 1632528060, 1632529860, 1632531660, 1632533460,
        1632535260, 1632537060, 1632538860, 1632540660, 1632542460, 1632544260,
        1632546060, 1632547860, 1632549660, 1632551460, 1632553260, 1632555060,
        1632556860, 1632558660, 1632560460, 1632562260, 1632564060, 1632565860,
        1632567660, 1632569460, 1632571260, 1632573060, 1632574860, 1632576660,
        1632578460, 1632580260, 1632582060, 1632583860, 1632585660, 1632587460,
        1632589260, 1632591060, 1632592860, 1632594660, 1632596460, 1632598260,
        1632600060, 1632601860, 1632603660, 1632605460, 1632607260, 1632609060,
        1632610860, 1632612660, 1632614460, 1632616260, 1632618060, 1632619860,
        1632621660, 1632623460, 1632625260, 1632627060, 1632628860, 1632630660,
        1632632460, 1632634260, 1632636060, 1632637860, 1632639660, 1632641460,
        1632643260, 1632645060, 1632646860, 1632648660, 1632650460, 1632652260,
        1632654060, 1632655860, 1632657660, 1632659460, 1632661260, 1632663060,
        1632664860, 1632666660, 1632668460, 1632670260, 1632672060, 1632673860,
        1632675660, 1632677460, 1632679260, 1632681060, 1632682860, 1632684660,
        1632686460, 1632688260, 1632690060, 1632691860, 1632693660, 1632695460,
        1632697260, 1632699060, 1632700860, 1632702660, 1632704460, 1632706260,
        1632708060, 1632709860, 1632711660, 1632713460, 1632715260, 1632717060,
        1632718860, 1632720660, 1632722460, 1632724260, 1632726060, 1632727860,
        1632729660, 1632731460, 1632733260, 1632735060, 1632736860, 1632738660,
        1632740460, 1632742260, 1632744060,
      ],
      rh: [
        45.52, 46.08, 45.48, 44.91, 45.3, 45.82, 47.76, 48.45, 50.16, 51, 50.98,
        52.8, 52.97, 53.91, 54.05, 54.27, 54.25, 54.27, 54.25, 54.29, 54.31,
        54.18, 54.07, 54.09, 54.01, 53.91, 53.85, 54.18, 53.96, 54.07, 53.99,
        53.39, 53.01, 53.53, 54.05, 54.9, 55.54, 56.2, 57.37, 56.91, 57.34,
        57.33, 56.26, 55.45, 54.81, 54.42, 53.81, 53.75, 53.48, 53.44, 53.75,
        53.73, 53.57, 52.99, 53.93, 54.14, 54.71, 54.94, 55.8, 56.24, 55.38,
        56.65, 55.79, 55.21, 55.02, 54.51, 54.18, 53.92, 53.75, 53.58, 53.56,
        53.41, 53.63, 53.63, 53.48, 53.89, 53.98, 53.87, 53.59, 53.09, 53.03,
        52.92, 53.59, 53.92, 53.69, 53.38, 52.8, 52.02, 51.16, 50.53, 50, 49.4,
        49.14, 48.84, 48.39, 48.23, 48.22, 48.06, 48.04, 48.33, 47.93, 47.77,
        48.03, 47.91, 49.77, 50.09, 50.98, 50.72, 51.36, 51.78, 52.08, 52.36,
        52.69, 52.91, 52.86, 53.03, 52.88, 53.12, 53.19, 53.16, 52.99, 52.16,
        51.15, 50.62, 50.33, 59.31, 67.55, 58.25, 62.4,
      ],
      airTemp: [
        22.5, 22.39, 22.35, 22.25, 22.23, 22.19, 22.12, 22.02, 21.97, 21.9,
        21.88, 21.87, 21.8, 21.8, 21.7, 21.63, 21.53, 21.42, 21.3, 21.18, 21.06,
        20.94, 20.79, 20.7, 20.57, 20.41, 20.24, 20.19, 20.13, 20.06, 20.06,
        20.06, 20.02, 19.96, 19.93, 19.92, 19.91, 19.91, 19.93, 20, 20.08, 20.2,
        20.32, 20.43, 20.57, 20.68, 20.81, 20.97, 21.08, 21.25, 21.39, 21.53,
        21.64, 21.76, 21.78, 21.8, 21.85, 21.87, 21.9, 21.88, 21.87, 21.78,
        21.76, 21.67, 21.6, 21.52, 21.43, 21.33, 21.23, 21.1, 21.05, 20.95,
        20.87, 20.78, 20.68, 20.61, 20.51, 20.5, 20.48, 20.46, 20.37, 20.34,
        20.26, 20.24, 20.2, 20.22, 20.2, 20.27, 20.33, 20.4, 20.46, 20.58,
        20.68, 20.82, 20.97, 21.09, 21.27, 21.42, 21.58, 21.64, 21.78, 21.81,
        21.82, 21.87, 21.85, 21.85, 21.88, 21.9, 21.9, 21.85, 21.8, 21.71,
        21.63, 21.53, 21.44, 21.35, 21.25, 21.15, 21.02, 20.92, 20.84, 20.7,
        20.61, 20.5, 20.44, 20.39, 20.39, 20.34, 20.4,
      ],
    },
  ];

 
  //will need to adjust the return type here. 
  let xAndyTraces = [];
  xAndyTraces.push(filter_sensor_data_for_unix_time(data, payload.exposureType, "unixTime"), 
  filter_sensor_data_for_within_range(data,payload.exposureType, payload.sensorDataName,  payload.dataSelections[0].minValue,  payload.dataSelections[0].maxValue))

  //unixTime is xAndyTraces[0]
  return xAndyTraces

}

function filter_sensor_data_for_within_range(sensorData, exposureType, filterFor, minValue, maxValue) {

  return filter_range(read_exposure_for(read_exposure_type(sensorData)));

  function read_exposure_type(sensorData) {
      if (sensorData && sensorData.length) {
          return sensorData.find(dataEntry => dataEntry.exposureType === exposureType) || {}; //returns the object where exposure type is eqaul to the submitted type
      }
      return {};
  }
  function read_exposure_for(exposureData) {
      return { exposureType: exposureData.exposureType, [filterFor]: exposureData[filterFor] || [] };
  }
  function filter_range(exposureDataProperty) {
      return [ [filterFor] = exposureDataProperty[filterFor].filter((temp) => temp >= minValue && temp <= maxValue) ];
      
      //keeping this as it returns the exposureType as well as the sensor data.
      // return {exposureType: exposureDataProperty.exposureType,  [filterFor]: exposureDataProperty[filterFor].filter((temp) => temp >= minValue && temp <= maxValue) };
  }
}

function filter_sensor_data_for_unix_time(sensorData, exposureType,  filterFor) {

  return filter_range(read_exposure_for(read_exposure_type(sensorData)));

  function read_exposure_type(sensorData) {
      if (sensorData && sensorData.length) {
          return sensorData.find(dataEntry => dataEntry.exposureType === exposureType) || {}; //returns the object where exposure type is eqaul to the submitted type
      }
      return {};
  }
  function read_exposure_for(exposureData) {
      return { exposureType: exposureData.exposureType, [filterFor]: exposureData[filterFor] || [] };
  }
  function filter_range(exposureDataProperty) {
      return [ [filterFor] = exposureDataProperty[filterFor].filter((temp) => temp) ];
      
      //keeping this as it returns the exposureType as well as the sensor data.
      // return {exposureType: exposureDataProperty.exposureType,  [filterFor]: exposureDataProperty[filterFor].filter((temp) => temp >= minValue && temp <= maxValue) };
  }
}

function convertDates(traceData){
  let convertedDates = [];
  for(var i=0; i<traceData[0].length; i++){
    const element = traceData[0][i]; //gets unixTime element
    var timestamp = element * 1000; 
    var date = new Date(timestamp);
    convertedDates.push(String(date.getMonth()).padStart(2, '0') + '-' + date.getDate() + '-' + date.getHours() + ':' + date.getMinutes + ':' + date.getSeconds)

  }
  return convertedDates;
  
      // console.log(date.getTime());
      // console.log(date.getDate());
      
}
