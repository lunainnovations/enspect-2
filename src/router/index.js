import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import SensorData from '../components/shared/SensorData/DataLayout.vue'
import DataOps from '../components/shared/DataOps/DataOperations.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/sensor-data',
    name: 'SensorData',
  
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/SensorData.vue'),
    component: SensorData,
    props: true
  },
  {
    path: '/data-operations',
    name: 'DataOperations',
    component : DataOps,
    props: true,
  }
]

const router = new VueRouter({
  routes
})

export default router
