# Summary

Date : 2022-04-05 13:55:34

Directory d:\Code\repos\Project Code\vue_enspect_2\src\components

Total : 22 files,  3564 codes, 117 comments, 250 blanks, all 3931 lines

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Vue | 21 | 3,376 | 85 | 206 | 3,667 |
| JavaScript | 1 | 188 | 32 | 44 | 264 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 22 | 3,564 | 117 | 250 | 3,931 |
| shared | 20 | 3,398 | 116 | 239 | 3,753 |
| shared\FormData | 15 | 2,456 | 49 | 141 | 2,646 |
| shared\FormData\Common | 9 | 1,767 | 47 | 102 | 1,916 |
| shared\FormData\ExposureTypeAssets | 3 | 481 | 0 | 27 | 508 |
| shared\FormData\SensorMeasurements | 2 | 193 | 2 | 8 | 203 |
| shared\SensorData | 4 | 917 | 67 | 94 | 1,078 |

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)