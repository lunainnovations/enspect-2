# Details

Date : 2022-04-05 13:55:34

Directory d:\Code\repos\Project Code\vue_enspect_2\src\components

Total : 22 files,  3564 codes, 117 comments, 250 blanks, all 3931 lines

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/components/EnspectFileInput.vue](/src/components/EnspectFileInput.vue) | Vue | 117 | 1 | 7 | 125 |
| [src/components/SensorSelect.vue](/src/components/SensorSelect.vue) | Vue | 49 | 0 | 4 | 53 |
| [src/components/shared/FormData/Common/AcuityCR.vue](/src/components/shared/FormData/Common/AcuityCR.vue) | Vue | 520 | 12 | 28 | 560 |
| [src/components/shared/FormData/Common/AcuityCrSensor1.vue](/src/components/shared/FormData/Common/AcuityCrSensor1.vue) | Vue | 146 | 7 | 11 | 164 |
| [src/components/shared/FormData/Common/AcuityCrSensor2.vue](/src/components/shared/FormData/Common/AcuityCrSensor2.vue) | Vue | 144 | 7 | 7 | 158 |
| [src/components/shared/FormData/Common/AcuityCrSensor3.vue](/src/components/shared/FormData/Common/AcuityCrSensor3.vue) | Vue | 144 | 7 | 9 | 160 |
| [src/components/shared/FormData/Common/AcuityDG.vue](/src/components/shared/FormData/Common/AcuityDG.vue) | Vue | 23 | 0 | 4 | 27 |
| [src/components/shared/FormData/Common/AcuityLS.vue](/src/components/shared/FormData/Common/AcuityLS.vue) | Vue | 23 | 0 | 7 | 30 |
| [src/components/shared/FormData/Common/AddNewCorrosionValueDialog.vue](/src/components/shared/FormData/Common/AddNewCorrosionValueDialog.vue) | Vue | 84 | 0 | 4 | 88 |
| [src/components/shared/FormData/Common/Coatings.vue](/src/components/shared/FormData/Common/Coatings.vue) | Vue | 118 | 0 | 10 | 128 |
| [src/components/shared/FormData/Common/RequiredInputs.vue](/src/components/shared/FormData/Common/RequiredInputs.vue) | Vue | 565 | 14 | 22 | 601 |
| [src/components/shared/FormData/ExposureTypeAssets/LabExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/LabExposureType.vue) | Vue | 129 | 0 | 5 | 134 |
| [src/components/shared/FormData/ExposureTypeAssets/OnAssetExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/OnAssetExposureType.vue) | Vue | 234 | 0 | 16 | 250 |
| [src/components/shared/FormData/ExposureTypeAssets/OutdoorExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/OutdoorExposureType.vue) | Vue | 118 | 0 | 6 | 124 |
| [src/components/shared/FormData/FormTitle.vue](/src/components/shared/FormData/FormTitle.vue) | Vue | 15 | 0 | 4 | 19 |
| [src/components/shared/FormData/SensorMeasurements/DG_SensorPanel.vue](/src/components/shared/FormData/SensorMeasurements/DG_SensorPanel.vue) | Vue | 72 | 1 | 3 | 76 |
| [src/components/shared/FormData/SensorMeasurements/LS_SensorPanel.vue](/src/components/shared/FormData/SensorMeasurements/LS_SensorPanel.vue) | Vue | 121 | 1 | 5 | 127 |
| [src/components/shared/SensorData/Data.js](/src/components/shared/SensorData/Data.js) | JavaScript | 188 | 32 | 44 | 264 |
| [src/components/shared/SensorData/DataFilters.vue](/src/components/shared/SensorData/DataFilters.vue) | Vue | 137 | 0 | 9 | 146 |
| [src/components/shared/SensorData/DataLayout.vue](/src/components/shared/SensorData/DataLayout.vue) | Vue | 541 | 35 | 32 | 608 |
| [src/components/shared/SensorData/SelectCoatingValuesDialog.vue](/src/components/shared/SensorData/SelectCoatingValuesDialog.vue) | Vue | 51 | 0 | 9 | 60 |
| [src/components/shared/Snackbar.vue](/src/components/shared/Snackbar.vue) | Vue | 25 | 0 | 4 | 29 |

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)