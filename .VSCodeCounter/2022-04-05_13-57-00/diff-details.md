# Diff Details

Date : 2022-04-05 13:57:00

Directory d:\Code\repos\Project Code\vue_enspect_2\src

Total : 11 files,  670 codes, 82 comments, 121 blanks, all 873 lines

[summary](results.md) / [details](details.md) / [diff summary](diff.md) / diff details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 66 | 0 | 14 | 80 |
| [src/alloys.json](/src/alloys.json) | JSON | 25 | 0 | 1 | 26 |
| [src/assets/logo.svg](/src/assets/logo.svg) | XML | 1 | 0 | 1 | 2 |
| [src/http-common.js](/src/http-common.js) | JavaScript | 6 | 0 | 2 | 8 |
| [src/main.js](/src/main.js) | JavaScript | 22 | 0 | 3 | 25 |
| [src/plugins/vuetify.js](/src/plugins/vuetify.js) | JavaScript | 5 | 0 | 3 | 8 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 21 | 3 | 6 | 30 |
| [src/services/UploadFileService.js](/src/services/UploadFileService.js) | JavaScript | 12 | 0 | 6 | 18 |
| [src/store/index.js](/src/store/index.js) | JavaScript | 484 | 79 | 73 | 636 |
| [src/views/Home.vue](/src/views/Home.vue) | Vue | 11 | 0 | 6 | 17 |
| [src/views/SensorData.vue](/src/views/SensorData.vue) | Vue | 17 | 0 | 6 | 23 |

[summary](results.md) / [details](details.md) / [diff summary](diff.md) / diff details