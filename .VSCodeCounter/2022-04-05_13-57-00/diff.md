# Diff Summary

Date : 2022-04-05 13:57:00

Directory d:\Code\repos\Project Code\vue_enspect_2\src

Total : 11 files,  670 codes, 82 comments, 121 blanks, all 873 lines

[summary](results.md) / [details](details.md) / diff summary / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 6 | 550 | 82 | 93 | 725 |
| Vue | 3 | 94 | 0 | 26 | 120 |
| JSON | 1 | 25 | 0 | 1 | 26 |
| XML | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 11 | 670 | 82 | 121 | 873 |
| assets | 1 | 1 | 0 | 1 | 2 |
| plugins | 1 | 5 | 0 | 3 | 8 |
| router | 1 | 21 | 3 | 6 | 30 |
| services | 1 | 12 | 0 | 6 | 18 |
| store | 1 | 484 | 79 | 73 | 636 |
| views | 2 | 28 | 0 | 12 | 40 |

[summary](results.md) / [details](details.md) / diff summary / [diff details](diff-details.md)