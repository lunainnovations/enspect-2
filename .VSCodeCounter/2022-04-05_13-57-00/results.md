# Summary

Date : 2022-04-05 13:57:00

Directory d:\Code\repos\Project Code\vue_enspect_2\src

Total : 33 files,  4234 codes, 199 comments, 371 blanks, all 4804 lines

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Vue | 24 | 3,470 | 85 | 232 | 3,787 |
| JavaScript | 7 | 738 | 114 | 137 | 989 |
| JSON | 1 | 25 | 0 | 1 | 26 |
| XML | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 33 | 4,234 | 199 | 371 | 4,804 |
| assets | 1 | 1 | 0 | 1 | 2 |
| components | 22 | 3,564 | 117 | 250 | 3,931 |
| components\shared | 20 | 3,398 | 116 | 239 | 3,753 |
| components\shared\FormData | 15 | 2,456 | 49 | 141 | 2,646 |
| components\shared\FormData\Common | 9 | 1,767 | 47 | 102 | 1,916 |
| components\shared\FormData\ExposureTypeAssets | 3 | 481 | 0 | 27 | 508 |
| components\shared\FormData\SensorMeasurements | 2 | 193 | 2 | 8 | 203 |
| components\shared\SensorData | 4 | 917 | 67 | 94 | 1,078 |
| plugins | 1 | 5 | 0 | 3 | 8 |
| router | 1 | 21 | 3 | 6 | 30 |
| services | 1 | 12 | 0 | 6 | 18 |
| store | 1 | 484 | 79 | 73 | 636 |
| views | 2 | 28 | 0 | 12 | 40 |

summary / [details](details.md) / [diff summary](diff.md) / [diff details](diff-details.md)