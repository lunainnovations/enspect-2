# Details

Date : 2022-04-05 13:57:00

Directory d:\Code\repos\Project Code\vue_enspect_2\src

Total : 33 files,  4234 codes, 199 comments, 371 blanks, all 4804 lines

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.vue](/src/App.vue) | Vue | 66 | 0 | 14 | 80 |
| [src/alloys.json](/src/alloys.json) | JSON | 25 | 0 | 1 | 26 |
| [src/assets/logo.svg](/src/assets/logo.svg) | XML | 1 | 0 | 1 | 2 |
| [src/components/EnspectFileInput.vue](/src/components/EnspectFileInput.vue) | Vue | 117 | 1 | 7 | 125 |
| [src/components/SensorSelect.vue](/src/components/SensorSelect.vue) | Vue | 49 | 0 | 4 | 53 |
| [src/components/shared/FormData/Common/AcuityCR.vue](/src/components/shared/FormData/Common/AcuityCR.vue) | Vue | 520 | 12 | 28 | 560 |
| [src/components/shared/FormData/Common/AcuityCrSensor1.vue](/src/components/shared/FormData/Common/AcuityCrSensor1.vue) | Vue | 146 | 7 | 11 | 164 |
| [src/components/shared/FormData/Common/AcuityCrSensor2.vue](/src/components/shared/FormData/Common/AcuityCrSensor2.vue) | Vue | 144 | 7 | 7 | 158 |
| [src/components/shared/FormData/Common/AcuityCrSensor3.vue](/src/components/shared/FormData/Common/AcuityCrSensor3.vue) | Vue | 144 | 7 | 9 | 160 |
| [src/components/shared/FormData/Common/AcuityDG.vue](/src/components/shared/FormData/Common/AcuityDG.vue) | Vue | 23 | 0 | 4 | 27 |
| [src/components/shared/FormData/Common/AcuityLS.vue](/src/components/shared/FormData/Common/AcuityLS.vue) | Vue | 23 | 0 | 7 | 30 |
| [src/components/shared/FormData/Common/AddNewCorrosionValueDialog.vue](/src/components/shared/FormData/Common/AddNewCorrosionValueDialog.vue) | Vue | 84 | 0 | 4 | 88 |
| [src/components/shared/FormData/Common/Coatings.vue](/src/components/shared/FormData/Common/Coatings.vue) | Vue | 118 | 0 | 10 | 128 |
| [src/components/shared/FormData/Common/RequiredInputs.vue](/src/components/shared/FormData/Common/RequiredInputs.vue) | Vue | 565 | 14 | 22 | 601 |
| [src/components/shared/FormData/ExposureTypeAssets/LabExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/LabExposureType.vue) | Vue | 129 | 0 | 5 | 134 |
| [src/components/shared/FormData/ExposureTypeAssets/OnAssetExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/OnAssetExposureType.vue) | Vue | 234 | 0 | 16 | 250 |
| [src/components/shared/FormData/ExposureTypeAssets/OutdoorExposureType.vue](/src/components/shared/FormData/ExposureTypeAssets/OutdoorExposureType.vue) | Vue | 118 | 0 | 6 | 124 |
| [src/components/shared/FormData/FormTitle.vue](/src/components/shared/FormData/FormTitle.vue) | Vue | 15 | 0 | 4 | 19 |
| [src/components/shared/FormData/SensorMeasurements/DG_SensorPanel.vue](/src/components/shared/FormData/SensorMeasurements/DG_SensorPanel.vue) | Vue | 72 | 1 | 3 | 76 |
| [src/components/shared/FormData/SensorMeasurements/LS_SensorPanel.vue](/src/components/shared/FormData/SensorMeasurements/LS_SensorPanel.vue) | Vue | 121 | 1 | 5 | 127 |
| [src/components/shared/SensorData/Data.js](/src/components/shared/SensorData/Data.js) | JavaScript | 188 | 32 | 44 | 264 |
| [src/components/shared/SensorData/DataFilters.vue](/src/components/shared/SensorData/DataFilters.vue) | Vue | 137 | 0 | 9 | 146 |
| [src/components/shared/SensorData/DataLayout.vue](/src/components/shared/SensorData/DataLayout.vue) | Vue | 541 | 35 | 32 | 608 |
| [src/components/shared/SensorData/SelectCoatingValuesDialog.vue](/src/components/shared/SensorData/SelectCoatingValuesDialog.vue) | Vue | 51 | 0 | 9 | 60 |
| [src/components/shared/Snackbar.vue](/src/components/shared/Snackbar.vue) | Vue | 25 | 0 | 4 | 29 |
| [src/http-common.js](/src/http-common.js) | JavaScript | 6 | 0 | 2 | 8 |
| [src/main.js](/src/main.js) | JavaScript | 22 | 0 | 3 | 25 |
| [src/plugins/vuetify.js](/src/plugins/vuetify.js) | JavaScript | 5 | 0 | 3 | 8 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 21 | 3 | 6 | 30 |
| [src/services/UploadFileService.js](/src/services/UploadFileService.js) | JavaScript | 12 | 0 | 6 | 18 |
| [src/store/index.js](/src/store/index.js) | JavaScript | 484 | 79 | 73 | 636 |
| [src/views/Home.vue](/src/views/Home.vue) | Vue | 11 | 0 | 6 | 17 |
| [src/views/SensorData.vue](/src/views/SensorData.vue) | Vue | 17 | 0 | 6 | 23 |

[summary](results.md) / details / [diff summary](diff.md) / [diff details](diff-details.md)